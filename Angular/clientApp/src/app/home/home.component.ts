import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Local } from 'protractor/built/driverProviders';
import { Router } from "@angular/router";
import { Student} from '../Student';
import { JSONP_ERR_WRONG_RESPONSE_TYPE } from '@angular/common/http/src/jsonp';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public student: any={ };


  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    var n = localStorage.length;
    
    
    if (n == 0) {
      this.router.navigate(['./login']);
    }
    else {
      var token = JSON.parse(localStorage.getItem('currentUser'));
      var httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token.token,
          'ResponseType':'Json'
        })
      };
      this.http.get('http://localhost:62864/api/account/get', httpOptions).subscribe(result => {
        if (result == "failed") {
          this.router.navigate(['./login']);
        }
        else {
          this.student = result;

        }
      });
    }

   

  }
  Logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['./login']);
  }
}

