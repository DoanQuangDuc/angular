export class Student {
  id: number;
  username: string;
  password: string;
  fullname: string;
  major: string;
}
