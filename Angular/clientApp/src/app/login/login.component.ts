import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Local } from 'protractor/built/driverProviders';
import { Router } from "@angular/router";



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public model = {
    Username: "",
    Password: ""
  }
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }


  onSubmit() {
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    console.log(httpOptions);
    this.http.post('http://localhost:62864/api/account/login', this.model, httpOptions).subscribe(result => {
      if (result == 'Could not verify username and password')
        alert(result);
      else {
        localStorage.setItem('currentUser', JSON.stringify(result));
        this.router.navigate(['/home']);


      }
    });
  }

}
